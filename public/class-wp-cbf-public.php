<?php
class Wp_Cbf_Public {
	//---------------------------------------
	private $plugin_name;
	private $version;
	private $post_type_service;
	private $post_type_client;
	private $post_type_brand;
	private $post_type_product;
	private $meta_product;
	private $meta_brand;
	private $meta_client;
	//---------------------------------------
	public function __construct( $plugin_name, $version ) {
		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}
	//---------------------------------------
	//---------------------------------------
	public function enqueue_styles() {
		//---------------------------------------
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wp-cbf-public.css', array(), $this->version, 'all' );
		//---------------------------------------
		wp_register_style( 'jquery.fancybox.min.css', esc_url_raw( 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.4/jquery.fancybox.min.css' ), array(), null );
		wp_enqueue_style( 'jquery.fancybox.min.css' );
		//---------------------------------------
	}
	//---------------------------------------
	//---------------------------------------
	public function enqueue_scripts() {
		wp_enqueue_script('jquery');
		//---------------------------------------
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wp-cbf-public.js', array( 'jquery' ), $this->version, false );
		//---------------------------------------
		$fancybox = 'jquery.fancybox.js';
		   	$list = 'enqueued';
		   	
		    if (wp_script_is( $fancybox, $list )) {
		     	return;
		    } else {
		       wp_register_script( 'jquery.fancybox.js', esc_url_raw( '//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.4/jquery.fancybox.js' ), array(), null );	
		       wp_enqueue_script( 'jquery.fancybox.js' );
		    }
	}
	//---------------------------------------
	//---------------------------------------
	public function showFancybox( $atts , $content = null)
	{
		self::enqueue_styles();
		self::enqueue_scripts();

		$sc_atts = shortcode_atts( array(
		        'title' => 'Try now',
		        'class' => 'showFancybox',
		        'scrolling' => 'no',
		        'titleShow' => 'false',
		        'style-button' => 'nectar-button'
		    ), $atts );
		
		$title = $sc_atts['title'];
		$class = $sc_atts['class'];
		$scrolling = $sc_atts['scrolling'];
		$titleShow = $sc_atts['titleShow'];

		$href = uniqid();
		$id_link = uniqid();


		add_action('wp_footer' , function() use ($id_link , $scrolling ,$titleShow) {
			ob_start();
			?>
			<script type="text/javascript">
				jQuery(document).ready(function($) {
					$("#<?=$id_link?>").fancybox({
						'scrolling'		: '<?=$scrolling?>',
						'titleShow'		: <?=$titleShow?>,
					});
				});
			</script>
			<?
			$ob_contents = ob_get_contents();
    		ob_end_clean();
    		echo $ob_contents;	
		});

		ob_start();
    		?>
    			<a class="nectar-button small see-through has-icon" 
    				href="#<?=$href?>"
    				data-color-override="false" 
    				data-hover-color-override="false" 
    				data-hover-text-color-override="#fff"
    				id="<?=$id_link?>"
    				>
    				<span><?=$title?></span>
    				<i class="icon-button-arrow"></i>
    			</a>
				<div style="display: none;">
					<div id="<?=$href?>" class="<?=$class?>">
						<?=do_shortcode($content)?>
					</div>
				</div>
    		<?
    	$ob_contents = ob_get_contents();
    	ob_end_clean();
    	return $ob_contents;
	} 
	//---------------------------------------
	//---------------------------------------
	public function showDonacionFormFancyBox ()
	{
		self::enqueue_styles();
		self::enqueue_scripts();
		ob_start();
		?>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$("a[href='#showDonacionFormFancyBox']").fancybox({
					'scrolling'	:  'no' ,
					'titleShow'	: false ,
				});
			});
		</script>
		<div style="display:none">
			<div id="showDonacionFormFancyBox" class="showDonacionFormFancyBoxContent">
				<?include plugin_dir_path( dirname( __FILE__ ) ) . 'public/views/v_showDonacionFormFancyBox.php';?>
			</div>
		</div>
		<?
		$ob_contents = ob_get_contents();
    	ob_end_clean();
    	echo $ob_contents;	
	}
	//---------------------------------------
	//---------------------------------------
}