<?
$plugin_base = "wp-content/plugins/hn-2017/";
$imgs_path = "public/assets/icons/";
?>
<div class="row-bg-wrap instance-1">
  <div class="col span_6">
    <div class="img-cont">
      <img src="<?=$plugin_base.$imgs_path.'donation-2.svg'?>" style="margin-bottom: .5em;">
    </div>
    <h3>Única ocasión</h3>
    <div class="spacer-1"></div>
    <!-- BEGIN : Form 1 -->
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
      <input type="hidden" name="cmd" value="_s-xclick">
      <input type="hidden" name="hosted_button_id" value="SLLR8JKEG97NJ">
      <input type="image" src="https://www.paypalobjects.com/es_XC/MX/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal, la forma más segura y rápida de pagar en línea.">
      <img alt="" border="0" src="https://www.paypalobjects.com/es_XC/i/scr/pixel.gif" width="1" height="1">
    </form>
    <!-- END : Form 1 -->
  </div>
  <div class="col span_6">
    <!-- BEGIN : Form 2 -->
    <div class="img-cont">
      <img src="<?=$plugin_base.$imgs_path.'donation-1.svg'?>">
    </div>
    <h3>Recurrente</h3>
    <center>
      <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_s-xclick">
        <input type="hidden" name="hosted_button_id" value="HCFZCWVJMEWP2">
        <table>
          <tr><td><input type="hidden" name="on0" value="Opciones de pago">Opciones de pago</td></tr><tr><td><select name="os0">
          <option value="Op1">Op1 : $50.00 MXN - mensual</option>
          <option value="Op2">Op2 : $100.00 MXN - mensual</option>
          <option value="Op3">Op3 : $500.00 MXN - mensual</option>
        </select> </td></tr>
      </table>
      <input type="hidden" name="currency_code" value="MXN">
      <input type="image" src="https://www.paypalobjects.com/es_XC/MX/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal, la forma más segura y rápida de pagar en línea.">
      <img alt="" border="0" src="https://www.paypalobjects.com/es_XC/i/scr/pixel.gif" width="1" height="1">
    </form>  
  </center>
  
  <!-- END : Form 2 -->
</div>
</div>

