<?php


class Wp_Tl {


	protected $plugin_name;
	protected $version;

	public function __construct() {
		
		$this->plugin_name = 'wp-tl';
		$this->version = '1.1.3';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		
	}

	
	private function load_dependencies() {

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wp-cbf-loader.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wp-cbf-i18n.php';
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-wp-cbf-admin.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-wp-cbf-public.php';

		$this->loader = new Wp_Cbf_Loader();

	}

	private function set_locale() {

		$plugin_i18n = new Wp_Cbf_i18n();
		$plugin_i18n->set_domain( $this->get_plugin_name() );
		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	private function define_admin_hooks() {
		$public_private = new Wp_Cbf_Admin( $this->get_plugin_name(), $this->get_version() );
	}

	private function define_public_hooks() {
		$plugin_public = new Wp_Cbf_Public( $this->get_plugin_name(), $this->get_version() );
		// -----------------------------
		// -----------------------------
		add_shortcode('show_fancybox' , array(&$plugin_public, 'showFancybox'));
		$this->loader->add_action( 'wp_footer', $plugin_public, 'showDonacionFormFancyBox');
		// -----------------------------
		// -----------------------------
	}
	

	public function run() {
		$this->loader->run();
	}
	
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	public function get_loader() {
		return $this->loader;
	}

	public function get_version() {
		return $this->version;
	}
	
}