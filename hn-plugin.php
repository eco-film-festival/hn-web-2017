<?
/**
* Plugin Name: Hombre Naturaleza  Plugin
* Plugin URI: http://krkn.solutions/
* Description: Hombre Naturaleza Web Site 2017
* Version: 1.1 
* Author: zowarin@gmail.com
* Author URI: zowarin@gmail.com
* License: GPL12
*/


if ( ! defined( 'WPINC' ) ) {
	die;
}

function activate_wp_cbf() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-activator.php';
	Wp_Activator::activate();
}

function deactivate_wp_cbf() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-deactivator.php';
	Wp_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wp_cbf' );
register_deactivation_hook( __FILE__, 'deactivate_wp_cbf');

require plugin_dir_path( __FILE__ ) . 'includes/class-wp-tl.php';


function run_wp_tl() {
	$plugin = new Wp_Tl();
	$plugin->run();
}
run_wp_tl();
?>